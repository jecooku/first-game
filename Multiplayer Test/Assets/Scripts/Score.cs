﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	int score = 0;
    Text countText;
    PhotonView myPhotonView;

    void Awake()
    {
        myPhotonView = GetComponent<PhotonView>();
        countText = GameObject.Find("Text").GetComponent<Text>() as Text;
    }

    void Start()
    {
        if (myPhotonView.isMine)
        {
            Debug.Log("vit");
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + score.ToString();
    }

    public void AddScore()
    {
        if (myPhotonView.isMine)
        {
            ++score;
            SetCountText();
        }
    }
}

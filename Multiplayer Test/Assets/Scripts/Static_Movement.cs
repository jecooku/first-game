﻿using UnityEngine;
using System.Collections;

public class Static_Movement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate(Vector3.right * Time.deltaTime * 30);
		this.transform.Rotate(Vector3.up * Time.deltaTime * 30);
	}
}

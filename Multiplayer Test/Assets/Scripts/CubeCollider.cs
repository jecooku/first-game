﻿using UnityEngine;
using System.Collections;

public class CubeCollider : MonoBehaviour {

    PhotonView myPhotonView;

    void OnTriggerEnter(Collider collider)
    {
        myPhotonView = GetComponent<PhotonView>();
        if (collider.tag == "Ball")
        {
            collider.GetComponent<Score>().AddScore();
            myPhotonView.RPC("DestroyObject", PhotonTargets.AllBuffered);
        }
    }

    [PunRPC]
    void DestroyObject()
    {
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.Destroy(gameObject);
        }
    }
}

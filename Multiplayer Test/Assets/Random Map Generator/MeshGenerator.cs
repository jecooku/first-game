﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshGenerator : MonoBehaviour
{
    public SquareGrid squareGrid;
    public MeshFilter walls;
    public MeshFilter cave;
    MeshCollider wallCollider;

    public bool is2D;

    List<Vector3> vertices;
    List<int> triangles;
    Dictionary<int, List<Triangle>> triangleDictionary = new Dictionary<int, List<Triangle>>(); // due to the nature of the algorithm, we need to know the triangles who own each vertice
    List<List<int>> outlines = new List<List<int>>();
    HashSet<int> checkedVertices = new HashSet<int>();

    public void GenerateMesh(int[,] map, float squareSize)
    {
        outlines.Clear();
        checkedVertices.Clear();
        triangleDictionary.Clear();

        squareGrid = new SquareGrid(map, squareSize);
        vertices = new List<Vector3>();
        triangles = new List<int>();

        for (int x = 0; x < squareGrid.squares.GetLength(0); x++)
        {
            for (int y = 0; y < squareGrid.squares.GetLength(1); y++)
            {
                TriangulateSquare(squareGrid.squares[x,y]);
            }
        }

        Mesh mesh = new Mesh();
        cave.mesh = mesh;

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        int tileAmount = 10;
        Vector2[] uvs = new Vector2[vertices.Count];
        for (int i =0; i< vertices.Count; i++)
        {
            float percentX = Mathf.InverseLerp(-map.GetLength(0) / 2 * squareSize, map.GetLength(0) / 2 * squareSize, vertices[i].x) * tileAmount;
            float percentY = Mathf.InverseLerp(-map.GetLength(1) / 2 * squareSize, map.GetLength(1) / 2 * squareSize, vertices[i].z) * tileAmount;
            uvs[i] = new Vector2(percentX, percentY);
        }

        mesh.uv = uvs;

        if (!is2D)
        {
            Destroy(wallCollider);
            CreateWallMesh();
        }
    }

    void CreateWallMesh()
    {
        CalculateMeshOutlines();
        List<Vector3> wallVertices = new List<Vector3>();
        List<int> wallTriangles = new List<int>();

        Mesh wallMesh = new Mesh();

        float wallHeight = 5;

        foreach (List<int> outline in outlines)
        {
            for(int i = 0; i < outline.Count - 1; i++)
            {
                int startinex = wallVertices.Count;
                wallVertices.Add(vertices[outline[i]]); // left
                wallVertices.Add(vertices[outline[i + 1]]); //right
                wallVertices.Add(vertices[outline[i]] - Vector3.up * wallHeight);
                wallVertices.Add(vertices[outline[i + 1]] - Vector3.up * wallHeight);

                wallTriangles.Add(startinex + 0);
                wallTriangles.Add(startinex + 2);
                wallTriangles.Add(startinex + 3);

                wallTriangles.Add(startinex + 3);
                wallTriangles.Add(startinex + 1);
                wallTriangles.Add(startinex + 0);
            }
        }

        wallMesh.vertices = wallVertices.ToArray();
        wallMesh.triangles = wallTriangles.ToArray();

        walls.mesh = wallMesh;

        wallCollider = walls.gameObject.AddComponent<MeshCollider>();
        wallCollider.sharedMesh = wallMesh;
    }

    void TriangulateSquare(Square square)
    {
        switch (square.configuration)
        {
            case 0:
                break;

            // single point selected
            case 1:
                CrateMeshFromPoints(square.centreLeft, square.centreBottom, square.lowerLeft);
                break;
            case 2:
                CrateMeshFromPoints(square.lowerRight, square.centreBottom, square.centreRight);
                break;
            case 4:
                CrateMeshFromPoints(square.upperRight, square.centreRight, square.centreTop);
                break;
            case 8:
                CrateMeshFromPoints(square.upperLeft, square.centreTop, square.centreLeft);
                break;

            // two points

            case 3:
                CrateMeshFromPoints(square.centreRight, square.lowerRight, square.lowerLeft, square.centreLeft);
                break;
            case 6:
                CrateMeshFromPoints(square.centreTop, square.upperRight, square.lowerRight, square.centreBottom);
                break;
            case 9:
                CrateMeshFromPoints(square.upperLeft, square.centreTop, square.centreBottom, square.lowerLeft);
                break;
            case 12:
                CrateMeshFromPoints(square.upperLeft, square.upperRight, square.centreRight, square.centreLeft);
                break;
            case 5:
                CrateMeshFromPoints(square.centreTop, square.upperRight, square.centreRight, square.centreBottom, square.lowerLeft, square.centreLeft);
                break;
            case 10:
                CrateMeshFromPoints(square.upperLeft, square.centreTop, square.centreRight, square.lowerRight, square.centreBottom, square.centreLeft);
                break;

            // 3 points

            case 7:
                CrateMeshFromPoints(square.centreTop, square.upperRight, square.lowerRight, square.lowerLeft, square.centreLeft);
                break;
            case 11:
                CrateMeshFromPoints(square.upperLeft, square.centreTop, square.centreRight, square.lowerRight, square.lowerLeft);
                break;
            case 13:
                CrateMeshFromPoints(square.upperLeft, square.upperRight, square.centreRight, square.centreBottom, square.lowerLeft);
                break;
            case 14:
                CrateMeshFromPoints(square.upperLeft, square.upperRight, square.lowerRight, square.centreBottom, square.centreLeft);
                break;

            // 4 points
            case 15:
                CrateMeshFromPoints(square.upperLeft, square.upperRight, square.lowerRight, square.lowerLeft);
                checkedVertices.Add(square.upperLeft.vertexIndex);
                checkedVertices.Add(square.upperRight.vertexIndex);
                checkedVertices.Add(square.lowerRight.vertexIndex);
                checkedVertices.Add(square.lowerLeft.vertexIndex);
                break;

        }
    }

    void CrateMeshFromPoints(params Node [] points)
    {
        AssignVertices(points);

        if(points.Length >= 3)
        {
            CreateTriangles(points[0], points[1], points[2]);
        }
        if (points.Length >= 4)
        {
            CreateTriangles(points[0], points[2], points[3]);
        }
        if (points.Length >= 5)
        {
            CreateTriangles(points[0], points[3], points[4]);
        }
        if (points.Length >= 6)
        {
            CreateTriangles(points[0], points[4], points[5]);
        }
    }

    void AssignVertices(Node [] points)
    {
        for(int i = 0; i <points.Length; i++)
        {
            if (points[i].vertexIndex == -1)
            {
                points[i].vertexIndex = vertices.Count;
                vertices.Add(points[i].position);
            }
        }
    }

    void CreateTriangles(Node a, Node b, Node c)
    {
        triangles.Add(a.vertexIndex);
        triangles.Add(b.vertexIndex);
        triangles.Add(c.vertexIndex);

        Triangle triangle = new Triangle(a.vertexIndex, b.vertexIndex, c.vertexIndex);
        AddTriangleToDictionary(a.vertexIndex, triangle);
        AddTriangleToDictionary(b.vertexIndex, triangle);
        AddTriangleToDictionary(c.vertexIndex, triangle);
    }

    void AddTriangleToDictionary(int vertexKey, Triangle triangle)
    {
        if (triangleDictionary.ContainsKey(vertexKey))
        {
            triangleDictionary[vertexKey].Add(triangle); // adds a triangle to the list of triangles that own the vertex
        }
        else
        {
            // otherwise, creates a new list of trinagles that have ownership over the vertex
            List<Triangle> nListOfTriangles = new List<Triangle>();
            nListOfTriangles.Add(triangle);
            triangleDictionary.Add(vertexKey, nListOfTriangles);
        }
    }

    void CalculateMeshOutlines()
    {
        for (int vertexIndex = 0; vertexIndex < vertices.Count; vertexIndex++)
        {
            if (!checkedVertices.Contains(vertexIndex))
            {
                int nOutlineVertex = GetConnectedOutlineVertex(vertexIndex);
                if(nOutlineVertex != -1)
                {
                    checkedVertices.Add(vertexIndex);

                    List<int> nOutline = new List<int>();
                    nOutline.Add(vertexIndex);
                    outlines.Add(nOutline);
                    FollowOutline(nOutlineVertex, outlines.Count - 1);
                    outlines[outlines.Count - 1].Add(vertexIndex);
                }
            }
        }
    }

    void FollowOutline(int vertexIndex, int outlineIndex)
    {
        outlines[outlineIndex].Add(vertexIndex);
        checkedVertices.Add(vertexIndex);
        int nextVertexindex = GetConnectedOutlineVertex(vertexIndex);

        if(nextVertexindex != -1)
        {
            FollowOutline(nextVertexindex, outlineIndex);
        }
    }

    int GetConnectedOutlineVertex(int verteIndex)
    {
        List<Triangle> trianglesContainingVertex = triangleDictionary[verteIndex];
        for (int i = 0; i < trianglesContainingVertex.Count; i++)
        {
            Triangle triangle = trianglesContainingVertex[i];

            for (int j = 0; j < 3; j++)
            {
                int vertexB = triangle[j];

                if (vertexB != verteIndex && !checkedVertices.Contains(vertexB))
                {
                    if (IsOutlineEdge(verteIndex, vertexB))
                    {
                        return vertexB;
                    }
                }
            }
        }

        return -1;
    }

    bool IsOutlineEdge(int vertexA, int vertexB)
    {
        List<Triangle> trianglesContainingA = triangleDictionary[vertexA];
        List<Triangle> trianglesContainingB = triangleDictionary[vertexB];
        int sharedTriangleCount = 0;

        for (int i=0; i < trianglesContainingA.Count; i++)
        {
            if (trianglesContainingA[i].Contains(vertexB))
            {
                sharedTriangleCount++;
                if (sharedTriangleCount < 1)
                {
                    break;
                }
            }
        }
        return sharedTriangleCount == 1;
    }

    struct Triangle
    {
        public int vertexA;
        public int vertexB;
        public int vertexC;
        int[] vertices;

        public Triangle(int a, int b, int c)
        {
            vertexA = a;
            vertexB = b;
            vertexC = c;

            vertices = new int[3];
            vertices[0] = a;
            vertices[1] = b;
            vertices[2] = c;
        }

        public int this[int i]
        {
            get { return vertices[i]; }
        }

        public bool Contains(int vertexIndex)
        {
            return vertexIndex == vertexA || vertexIndex == vertexB || vertexIndex == vertexC;
        }
    }

    public class Square
    {

        public ControlNode upperLeft, upperRight, lowerRight, lowerLeft;
        public Node centreTop, centreRight, centreBottom, centreLeft;
        public int configuration;

        public Square(ControlNode upperLeft, ControlNode upperRight, ControlNode lowerRight, ControlNode lowerLeft)
        {
            // refer to https://en.wikipedia.org/wiki/Marching_squares

            this.upperLeft = upperLeft;
            this.upperRight = upperRight;
            this.lowerRight = lowerRight;
            this.lowerLeft = lowerLeft;

            centreTop = upperLeft.right;
            centreRight = lowerRight.top;
            centreBottom = lowerLeft.right;
            centreLeft = lowerLeft.top;

            if (upperLeft.active)
                configuration += 8;
            if (upperRight.active)
                configuration += 4;
            if (lowerRight.active)
                configuration += 2;
            if (lowerLeft.active)
                configuration += 1;
            
        }
    }

    public class Node
    {
        public Vector3 position;
        public int vertexIndex = -1;

        public Node(Vector3 pos)
        {
            position = pos;
        }
    }

    public class ControlNode : Node
    {

        public bool active;
        public Node top, right;

        public ControlNode(Vector3 pos, bool active, float squareSize) : base(pos)
        {
            this.active = active;
            top = new Node(position + Vector3.forward * squareSize / 2f);
            right = new Node(position + Vector3.right * squareSize / 2f);
        }

    }

    public class SquareGrid
    {
        public Square[,] squares;

        public SquareGrid(int[,] map, float squareSize)
        {
            int numberOfNodesX = map.GetLength(0);
            int numberOfNodesY = map.GetLength(1);
            float mapWidth = numberOfNodesX * squareSize;
            float mapHeight = numberOfNodesY * squareSize;

            ControlNode[,] controlNodes = new ControlNode[numberOfNodesX, numberOfNodesY];

            for (int x = 0; x < numberOfNodesX; x++)
            {
                for (int y = 0; y < numberOfNodesY; y++)
                {
                    Vector3 pos = new Vector3(-mapWidth / 2 + x * squareSize + squareSize / 2, 0, -mapHeight / 2 + y * squareSize + squareSize / 2);
                    controlNodes[x, y] = new ControlNode(pos, map[x, y] == 1, squareSize);
                }
            }

            squares = new Square[numberOfNodesX - 1, numberOfNodesY - 1];
            for (int x = 0; x < numberOfNodesX - 1; x++)
            {
                for (int y = 0; y < numberOfNodesY - 1; y++)
                {
                    squares[x, y] = new Square(controlNodes[x, y + 1], controlNodes[x + 1, y + 1], controlNodes[x + 1, y], controlNodes[x, y]);
                }
            }

        }
    }
}


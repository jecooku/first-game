﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {

	private const string VERSION = "V0.1";
	public string roomName = "testRoom";
	public string playerPrefabName = "PlayerBall";
	public Transform spawnPoint; 
	public Transform brick;
	
	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings (VERSION);
    }
	
	void OnJoinedLobby(){
		RoomOptions roomOptions = new RoomOptions() { isVisible = false, maxPlayers = 4 };
		PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
	}
	
	void OnJoinedRoom(){
        GameObject player = PhotonNetwork.Instantiate (playerPrefabName, spawnPoint.position, spawnPoint.rotation, 0);
        player.GetComponent<Control>().enabled = true;
        player.GetComponent<Control>().isControllable = true;
		if (PhotonNetwork.isMasterClient) {
			for (int z = -8; z < 8; z=z+2) {
				for (int x = -8; x < 8; x=x+2) {
                    PhotonNetwork.InstantiateSceneObject("Cubo", new Vector3(x, 1, z), Quaternion.identity, 0, null);
				}
			}
		}
    }
}
